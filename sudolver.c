/*
 * sudolver - Sudoku solver
 * Licensed under BSD license:
 *
 * Copyright © 2018 Jan Krüger <jk@jk.gs>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <limits.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

/*
 * This solver is implemented for square Sudokus. It will probably work for
 * sizes other than 9x9 as long as it's a square if you change the BOX_WIDTH
 * constant below, but the pretty-printing output only supports 9x9 as of now.
 *
 * A board is stored as a flat array of integers; this simplifies access and
 * copying considerably. Given zero-based indexing, we can index into it using
 * the classic 'WIDTH*row + column' formula, implemented in the CELL macro
 * below. There are also helper macros to find the next/previous box in a
 * row or column given a base row/column.
 *
 * A board also stores a list of possible candidates for each field as a bit
 * mask of 9 bits. The most significant bit marks the digit '9' as a possible
 * candidate; the least significant bit marks the digit '1' as a possible
 * candidate.
 *
 * To improve speed, we also count remaining candidates for each cell and the
 * total number of solved cells in the board.
 *
 * Finally, the board has an 'invalid' flag that gets set as soon as all
 * candidates in a cell are eliminated, leaving the board unsolvable.
 */

/*
 * Overall implementation idea:
 * - Whenever a cell is set to a specific value, eliminate that number from
 *   all related cells. Should any of those cells have only one candidate
 *   left, set that cell, too. Keep repeating these two operations until there
 *   are no more updates to be made. This step is enough for easy Sudokus, and
 *   we can terminate here if we find a solution.
 *
 * - Pick a cell and try all possible values for it in turn.
 *   The algorithm to choose a cell can be customized using the
 *   SUDOLVER_SMART_BRANCH environment variable. The different algorithms are
 *   explained further below.
 *   For each guess, create a copy of the board, fill in the guess, and
 *   recursively repeat the overall procedure.
 *
 * - Once a solution has been found, if we needed to make a guess, backtrack
 *   to the last guess where we can still try more candidates and try to find
 *   a solution for that guess, too, using the same type of recursion.
 *   As soon as a second solution shows up, it will get printed, too (to prove
 *   that the puzzle has more than one solution). The limit of 2 is
 *   configurable using the env variable SUDOLVER_MAX, and if you'd like to
 *   know how many solutions a puzzle has even if you don't care about seeing
 *   all of them, you can set SUDOLVER_CHECK_TOTAL=1. Counting the solutions
 *   may take a long time for a underdetermined Sudoku.
 *
 * - Terminate if there are no more possible guesses.
 */

/****************************************************************************
 *    GENERAL DEFINITIONS FOR BOARD                                         *
 ****************************************************************************/

#define BOX_WIDTH 3
#define WIDTH (BOX_WIDTH*BOX_WIDTH)
/* Do not attempt to find more than this many solutions */
#define SOLVED(board) (board->num_solved == WIDTH*WIDTH)

/*
 * Helpers to convert between cell indexes (0..80) and x/y coordinates
 * x = column (0..9), y = row (0..9)
 */
#define CELL(x,y) (x + WIDTH * y)
#define CELL_X(cell) (cell % WIDTH)
#define CELL_Y(cell) (cell / WIDTH)
/* Gives the offset of the box into the current row/column of boxes, given an
 * x/y coordinate.
 */
#define BOX(z) (z / BOX_WIDTH)
/* Are two cells in the same box? */
#define SAMEBOX(a, b) (BOX(CELL_X(a)) == BOX(CELL_X(b)) && BOX(CELL_Y(a)) == BOX(CELL_Y(b)))

/* Get the bitmask for a single digit ('1' -> 0x1, '9' => 0x100) */
#define DIGIT_MASK(d) (1 << (d-1))

typedef struct {
	unsigned board[WIDTH*WIDTH];
	unsigned candidates[WIDTH*WIDTH];
	unsigned num_candidates[WIDTH*WIDTH];
	unsigned num_solved;
	unsigned invalid;
} board;

/****************************************************************************
 *    DEFINITIONS FOR RELATED CELLS ("ref")                                 *
 ****************************************************************************/

/*
 * The set of related cells for each cell is calculated at start-up so we
 * don't have to include huge hard-coded tables in the code. Each cell has 20
 * related cells: eight in the same box, six more in the same row, and six
 * more in the same column.
 * We determine this set of related cells for each cell, making for 9*9 sets
 * of 20 references each.
 *
 * ref[0..19] = related cells for cell (0, 0) = top left
 * ref[20..39] = related cells for cell (0, 1) = row 1 column 2
 * etc.
 *
 * (The above describes the situation for 9x9 Sudokus, it works similarly for
 * other sizes.)
 */

/* Number of related cells for each cell
 * The sum is made up of:
 *               .- related same box --.   .-- related row, column --.
 */
#define REF_MAX ((BOX_WIDTH*BOX_WIDTH-1) + (BOX_WIDTH*(BOX_WIDTH-1))*2)
/* A base offset into the reference array for a given cell index */
#define REF_BASE(idx) (idx * REF_MAX)


/****************************************************************************
 *    GLOBAL VARIABLES                                                      *
 ****************************************************************************/

static int (*cell_find_uncertain)(board *);

static unsigned ref[WIDTH*WIDTH * REF_MAX];

static int digits_provided;
static unsigned long long total_solves;
static int pending_guesses;
static unsigned long long total_guesses;
static int highest_depth;

static board *main_board;
static int input_chars;
static int pretty;
static int do_debug;
static int check_total;
static unsigned solve_max = 2;

/****************************************************************************
 *    HELPER FUNCTIONS                                                      *
 ****************************************************************************/

/* Pre-calculates the reference array */
void ref_setup()
{
	for (int cell = 0; cell < WIDTH*WIDTH; cell++) {
		int base = REF_BASE(cell);
		int idx = 0;
		for (int related = 0; related < WIDTH*WIDTH; related++) {
			if (related == cell) continue; /* Don't want to be self-referential */
			if (CELL_X(related) == CELL_X(cell) ||
				CELL_Y(related) == CELL_Y(cell) ||
				SAMEBOX(related, cell))
					ref[base + idx++] = related;
		}
	}
}

void info(char *msg, ...)
{
	va_list va;
	va_start(va, msg);
	vfprintf(stderr, msg, va);
	va_end(va);
}

void debug(char *msg, ...)
{
	if (!do_debug) return;
	va_list va;
	va_start(va, msg);
	vfprintf(stderr, msg, va);
	va_end(va);
}

/****************************************************************************
 *    BOARD/CELL FUNCTIONS                                                  *
 ****************************************************************************/

/* Initialize an empty board. */
board *board_init()
{
	int idx;
	board *b = calloc(1, sizeof (board));
	for (idx = 0; idx < WIDTH*WIDTH; idx++) {
		/* Mark all candidates viable */
		b->candidates[idx] = 0x1ff; /* = 0b111111111, 9 digits */
		b->num_candidates[idx] = 9;
		b->num_solved = 0;
	}
	return b;
}

/* Copies the full state of a board for recursion. */
board *board_clone(board *b)
{
	board *new = malloc(sizeof (board));
	memcpy(new, b, sizeof (board));
	return new;
}

void _board_print_rule(FILE *f, char corner, char fill, int candidates)
{
	int box, column;
	int box_size;
	if (candidates)
		/* candidates per row * cells per row + separators */
		box_size = BOX_WIDTH*BOX_WIDTH + BOX_WIDTH-1;
	else
		box_size = BOX_WIDTH;
	/* Prints a separator line for a standard or candidates grid. */
	for (box = 0; box < BOX_WIDTH; box++) {
		fputc(corner, f);
		for (column = 0; column < box_size; column++)
			fputc(fill, f);
	}
	fprintf(f, "%c\n", corner);
}

void board_print(board *b, FILE *f)
{
	for (int row = 0; row < WIDTH; row++) {
		if (pretty && !(row % BOX_WIDTH)) _board_print_rule(f, '+', '-', 0); /* End of boxes */
		for (int col = 0; col < WIDTH; col++) {
			if (pretty && !(col % BOX_WIDTH)) fputc('|', f);
			putchar(b->board[CELL(col, row)] + 0x30); /* ASCII digits = 0x30 ... 0x39 */
		}
		if (pretty)
			fputs("|\n", f);
		else
			fputc('\n', f);
	}
	if (pretty) {
		_board_print_rule(f, '+', '-', 0);
	}
	fputc('\n', f);
}

void debug_board_print_candidates(board *b)
{
	int row, col, subrow, digit;
	if (!do_debug) return;
	for (row = 0; row < WIDTH; row++) {
		if (row % BOX_WIDTH)
			_board_print_rule(stderr, '|', '.', 1);
		else
			_board_print_rule(stderr, '+', '-', 1);
		for (subrow = 0; subrow < BOX_WIDTH; subrow++) {
			for (col = 0; col < WIDTH; col++) {
				int cell = CELL(col, row);
				if (col == 0)
					fputc('|', stderr);
				else
					fputc('\'', stderr);
				for (digit = subrow*BOX_WIDTH + 1; digit < subrow*BOX_WIDTH + 4; digit++)
					fputc(b->candidates[cell] & DIGIT_MASK(digit) ? digit + '0' : ' ', stderr);
			}
			fputs("|\n", stderr);
		}
	}
	_board_print_rule(stderr, '+', '-', 1);
	info("Solved cells: ");
	if (b->invalid)
		info("[INVALID]\n\n");
	else
		info("%d\n\n", b->num_solved);
}

/* Finds the lowest digit that is still possible for a given bitmask. */
int mask_next_candidate(int mask)
{
	int digit = 1;
	while (mask) {
		if (mask & 0x1) return digit;
		mask >>= 1;
		digit++;
	}
	/* Nothing found */
	return 0;
}

/* The following functions implement various strategies for choosing a cell to
 * make a guess in.
 *
 * The basic strategy is to just pick the first field in which there are
 * multiple possible choices. This is a decent strategy.
 *
 * The 'min' strategy finds a field that has a minimum number of possible
 * values (ideally 2 but it will take what it can get). The idea with this is
 * that this reduces the branching factor in our recursive search, and
 * hopefully eliminates larger parts of the search space more quickly.
 *
 * As an opposite, the 'max' strategy finds a field that has a maximum number
 * of possible values (9 if it can). This will tend to maximize the number of
 * guesses needed and therefore usually make solving the puzzle slower. It is
 * implemented for experimental purposes.
 */

/* Finds an arbitrary cell with multiple candidates. */
int cell_find_uncertain_any(board *b)
{
	for (int cell = 0; cell < WIDTH*WIDTH; cell++)
		if (b->num_candidates[cell] > 1) return cell;
	return -1;
}

/*
 * Finds a cell that has the least count of viable candidates (but is still
 * uncertain). This is used to reduce the branching factor when testing
 * multiple solutions.
 */
int cell_find_uncertain_min_branch(board *b)
{
	int best_idx = -1;
	unsigned best_count = 9;
	for (int cell = 0; cell < WIDTH*WIDTH; cell++) {
		if (b->num_candidates[cell] == 1) continue;
		if (b->num_candidates[cell] >= best_count) continue;
		best_count = b->num_candidates[cell];
		best_idx = cell;
	}
	return best_idx;
}

/*
 * Finds a cell with the highest count of viable candidates.
 * This is used to increase the branching factor when testing
 * multiple solutions.
 */
int cell_find_uncertain_max_branch(board *b)
{
	int best_idx = -1;
	unsigned best_count = 1;
	for (int cell = 0; cell < WIDTH*WIDTH; cell++) {
		if (b->num_candidates[cell] <= best_count) continue;
		best_count = b->num_candidates[cell];
		best_idx = cell;
	}
	return best_idx;
}

void cell_set(board*, int, int);

/*
 * Marks a certain digit as not being a candidate for a given cell.
 * Automatically solves the cell if only one candidate is left, and also
 * detects whether the solution is now confirmed invalid.
 */
void cell_exclude_candidate(board *b, int cell, int digit)
{
	int num_candidates;
	/* Candidate already excluded? */
	if (!(b->candidates[cell] & DIGIT_MASK(digit))) return;

	if (b->num_candidates[cell] == 1) {
		/*
		 * This was our last candidate for the cell, so the board is
		 * now clearly invalid. Abort mission!
		 */
		b->invalid = 1;
		return;
	}

	b->candidates[cell] &= ~DIGIT_MASK(digit);
	num_candidates = --(b->num_candidates[cell]);
	if (num_candidates == 1) {
		cell_set(b, cell, mask_next_candidate(b->candidates[cell]));
	}
}

/*
 * Sets a cell to a fixed value (checking for consistency with which
 * candidates should be possible there). Automatically updates the candidates
 * for each related cell.
 * cell_exclude_candidate and cell_set call each other, so either of the two
 * will keep propagating updates to various cells until the whole thing
 * stabilizes, i.e. until we need more advanced strategies or guessing to
 * proceed.
 */
void cell_set(board *b, int cell, int digit)
{
	int ref_idx;
	if (!(b->candidates[cell] & DIGIT_MASK(digit))) {
		/*
		 * Setting a digit that isn't a valid candidate: board invalid
		 */
		b->invalid = 1;
		return;
	}
	if (b->board[cell] > 0) return;

	b->board[cell] = digit;
	b->candidates[cell] = DIGIT_MASK(digit);
	b->num_candidates[cell] = 1;
	b->num_solved++;
	for (ref_idx = 0; ref_idx < REF_MAX; ref_idx++)
		cell_exclude_candidate(b, ref[REF_BASE(cell) + ref_idx], digit);
}

/****************************************************************************
 *     RECURSION/MAIN CODE                                                  *
 ****************************************************************************/

void check_interesting()
{
	if (digits_provided >= 17) return;
	if (total_solves != 1) return;
	info("ATTENTION: you discovered a Sudoku with less than 17 initial hints\n"
		"(%d in fact) that has a unique solution. There's someone who will want to know:\n"
		"http://staffhome.ecm.uwa.edu.au/~00013890/sudokumin.php\n"
		"Please consider sending that person an e-mail!\n");
}

/*
 * Recursively solves a board. Creates cloned boards on each recursion step.
 * The recursion depth is bounded by the number of cells, so there will exist
 * at most 81 copies of the board at any given time, and 81 stack entries from
 * this function. This should be manageable on virtually any machine, even
 * many mobile devices.
 */
void board_solve_recursive(board *b, int depth)
{
	board *attempt;
	int try_cell;
	int try_digit;
	while (1) {
		if (b->invalid)
			return;
		if (depth > highest_depth) highest_depth = depth;
		if (b->invalid) return;
		if (SOLVED(b)) {
			if (total_solves == solve_max) {
				info("This board has more solutions than our limit of %u.\n", solve_max);
				if (check_total)
					info("I will keep searching to give you a total number of solutions, but\nno more solutions will be printed.\n");
				else
					info("If you want me to count all solutions, run sudolver like this:\nSUDOLVER_CHECK_TOTAL=1 ./sudolver\n");
				info("You can run sudolver with a higher limit like this:\nSUDOLVER_MAX=12345 ./sudolver\n");
				if (!check_total) exit(0);
			} else if (total_solves == 0 && depth == 0) {
				info("Single unique solution found without advanced strategies\n");
				board_print(b, stdout);
				check_interesting();
				exit(0);
			} else if (solve_max == 1) {
				info("Found a solution - not checking for more solutions because you set SUDOLVER_MAX=1\n");
				board_print(b, stdout);
				exit(0);
			}
			total_solves++;
			total_guesses += pending_guesses;
			pending_guesses = 0;
			if (total_solves <= solve_max) {
				info("Found solution #%llu, %llu guesses needed so far, recursion depth %d\n", total_solves, total_guesses, depth);
				board_print(b, stdout);
			}
			if (!(total_solves % 100000)) {
				info("Progress report: %llu solutions found so far, based on %llu guesses. %d cells fully tested.\n", total_solves, total_guesses, main_board->num_solved);
			}
			return;
		}
		try_cell = cell_find_uncertain(b);
		try_digit = mask_next_candidate(b->candidates[try_cell]);
		attempt = board_clone(b);
		cell_set(attempt, try_cell, try_digit);
		pending_guesses++;
		debug("Testing with cell #%d set to %d\n", try_cell, try_digit);
		debug_board_print_candidates(attempt);
		board_solve_recursive(attempt, depth+1);
		free(attempt);
		/*
		 * We've tried that digit and found all solutions with it...
		 * now look for solutions using other digits
		 */
		cell_exclude_candidate(b, try_cell, try_digit);
	}
}

int main(int argc, char *argv[])
{
	if (getenv("SUDOLVER_PRETTY") && !strcmp(getenv("SUDOLVER_PRETTY"), "1"))
		pretty = 1;
	if (getenv("SUDOLVER_DEBUG") && !strcmp(getenv("SUDOLVER_DEBUG"), "1"))
		do_debug = 1;
	if (getenv("SUDOLVER_MAX")) {
		char *max_start, *max_end;
		max_start = getenv("SUDOLVER_MAX");
		if (*max_start == '\0') {
			info("Empty SUDOLVER_MAX given, ignoring");
		} else {
			long max = strtol(getenv("SUDOLVER_MAX"), &max_end, 10);
			if (*max_end != '\0') {
				info("Invalid value for SUDOLVER_MAX, must be a positive number");
				exit(1);
			}
			if (max > (long) UINT_MAX) {
				info("Maximum supported value for SUDOLVER_MAX is %u", UINT_MAX);
				exit(1);
			}
			solve_max = (unsigned) max;
		}
	}
	if (getenv("SUDOLVER_CHECK_TOTAL") && !strcmp(getenv("SUDOLVER_CHECK_TOTAL"), "1"))
		check_total = 1;
	cell_find_uncertain = cell_find_uncertain_min_branch;
	if (getenv("SUDOLVER_SMART_BRANCH")) {
		if (!strcmp(getenv("SUDOLVER_SMART_BRANCH"), "min"))
			cell_find_uncertain = cell_find_uncertain_min_branch;
		else if (!strcmp(getenv("SUDOLVER_SMART_BRANCH"), "max"))
			cell_find_uncertain = cell_find_uncertain_max_branch;
		else if (!strcmp(getenv("SUDOLVER_SMART_BRANCH"), "any"))
			cell_find_uncertain = cell_find_uncertain_any;
	}

	main_board = board_init();
	ref_setup();

	if (isatty(fileno(stdin)))
		info("Please enter a Sudoku board:\n"
			"  * Space (' ') for undetermined cells\n"
			"  * Digits 1-9 for pre-determined cells\n"
			"  * Terminate with a line break or EOF\n"
			"  * Any other characters (extra newlines, punctuation, letters) are ignored.\n"
			"  * Extraneous digits and space are ignored.\n\n");

	while (input_chars < WIDTH*WIDTH) {
		int c = fgetc(stdin);
		if (c == ' ') {
			input_chars++;
		} else if (c >= '1' && c <= '9') {
			cell_set(main_board, input_chars++, c - '0'); /* ASCII character to digit by subtracting base */
			digits_provided++;
		} else if (c == EOF) {
			info("Early EOF, Sudoku definition is incomplete... aborting!\n");
			exit(0);
		}
	}

	if (digits_provided == 0) {
		info("Come on now, no hints at all? What am I supposed to do with that?");
		exit(1);
	}
	if (main_board->invalid) {
		info("The Sudoku you provided does not have a solution... aborting!\n");
		exit(1);
	}
	if (digits_provided < 17) {
		info("Warning: you gave %d hints.\n"
			"A common hypothesis says that at least 17 hints are required for a unique\n"
			"solution. Continuing anyway, though.\n\n");
	}
	board_solve_recursive(main_board, 0);
	if (total_solves == 0) {
		info("Could not find any solutions for this Sudoku, even though I tried everything. :(\n");
	} else {
		info("All %llu solutions found! Highest recursion depth was %d.\n", total_solves, highest_depth);
	}
	check_interesting();
}
